Tinusaur Arduino IDE Boards - Support files for the Arduino IDE

-----------------------------------------------------------------------------------
 Copyright (c) 2021 The Tinusaur Team. All Rights Reserved.
 Distributed as open source software under MIT License, see LICENSE.txt file.
 Please, as a favor, retain the link http://tinusaur.org to The Tinusaur Project.
-----------------------------------------------------------------------------------

 (*)  Under MIT License except where otherwise specified.
 (**) The "variants/tiny8/pins_arduino.h" file is under the GNU Lesser General Public License
      Source: https://github.com/damellis/attiny/blob/master/variants/tiny8/pins_arduino.h

==== Description ====

Tinusaur boards support files for the Arduino IDE. Mostly text files.

This README.TXT file URL:
	https://gitlab.com/tinusaur/arduino-ide-boards/-/blob/master/README.txt

3d party boards support file URL:
	https://gitlab.com/tinusaur/arduino-ide-boards/-/raw/master/package_tinusaur_attiny_index.json

==== Links ====

Official Tinusaur Project website: http://tinusaur.org
Arduino IDE Setup Guide for Tinusaur Boards: http://tinusaur.org/arduino/arduino-ide-setup/
Project Arduino IDE Boards source code: https://gitlab.com/tinusaur/arduino-ide-boards

Twitter: https://twitter.com/tinusaur
Facebook: https://www.facebook.com/tinusaur

==== References ====

The "variants/tiny8/pins_arduino.h" file by David A. Mellis
https://github.com/damellis/attiny/

Unofficial list of 3rd party boards support URLs
https://github.com/arduino/Arduino/wiki/Unofficial-list-of-3rd-party-boards-support-urls

SpenceKonde/ATTinyCore: Arduino core for ATtiny 1634, 828, x313, x4, x41, x5, x61, x7 and x8
https://github.com/SpenceKonde/ATTinyCore
NOTE: Look at the forks for useful changes and additions.

